var express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'),
	cons = require('consolidate'),
	dust = require('dustjs-helpers'),
	pg = require('pg'),
	app = express();

// DB Connect String
const conString = 'postgres://lucaszsf:241066@localhost/recipebook';

// Assign Dust Engine To .dust Files
app.engine('dust', cons.dust);

// Set Default Ext .dust
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Set Public Folder
app.use(express.static(path.join(__dirname, 'public')));

// Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

app.get('/', function(req, res){
	// Pg Connect
	pg.connect(conString, function(err, client, done){
		if(err) throw err;
		client.query('select * from recipes', function(err, result){
			if (err) throw err;
			res.render('index', {recipes: result.rows});
			done();
		});
	});
});

app.post('/add', function(req, res){
	pg.connect(conString, function(err, client, done){
		if(err) throw err;
		client.query('insert into recipes(name, ingredients, directions) values ($1, $2, $3)', [req.body.name, req.body.ingredients, req.body.directions]);
		done();
		res.redirect('/');
	});
});

app.delete('/delete/:id', function(req, res){
	pg.connect(conString, function(err, client, done){
		if(err) throw err;
		client.query('delete from recipes where id = $1', [req.params.id]); 
		done();
		res.send(200);
	});
});

app.post('/edit', function(req, res){
	pg.connect(conString, function(err, client, done){
		if(err) throw err;
		client.query('update recipes set name=$1, ingredients=$2, directions=$3 where id=$4', [req.body.name, req.body.ingredients, req.body.directions, req.body.id]);
		done();
		res.redirect('/');
	});
});

// Server
app.listen(3000, function(){
	console.log('Server Started On Port 3000');
});
